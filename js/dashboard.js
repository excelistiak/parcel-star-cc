// const menu = document.getElementById("menu");
// const close = document.getElementById("close-menu");
// const navbar = document.getElementById("navbar");
// menu.addEventListener("click", () => {
//   navbar.classList.remove("hide");
//   navbar.classList.add("show");
// });
// close.addEventListener("click", () => {

//   navbar.classList.add("hide");
//   navbar.classList.remove("show");
// });
// addEventListener("resize", () => {
//   navbar.classList.remove("show");
//   navbar.classList.remove("hide");
//   if (window.innerWidth < 1220) {
//     // navbar.classList.remove("show")
//     navbar.classList.add("hide");
//   } else {
//     navbar.classList.remove("hide");
//   }
// });
// addEventListener("load",()=>{
//   if (window.innerWidth < 1220) {
//     navbar.classList.remove("show")
//     navbar.classList.add("hide");
//   } else {
//     navbar.classList.remove("show");
//     navbar.classList.remove("hide");

//   }
// })


const menu = document.getElementById("menu");
const close = document.getElementById("close-menu");
const navbar = document.getElementById("navbar");
menu.addEventListener("click", () => {
  navbar.classList.remove("hide");
  navbar.classList.add("show");
});
close.addEventListener("click", () => {

  navbar.classList.add("hide");
  navbar.classList.remove("show");
});
addEventListener("resize", () => {
  navbar.classList.remove("show");
  navbar.classList.remove("hide");
  const dashboard=document.getElementById('dashboard-container')
  if (window.innerWidth < 900) {
    // navbar.classList.remove("show")
    navbar.classList.add("hide");
    dashboard.style.gridTemplateColumns="0 1fr";
  } else {
    navbar.classList.remove("hide");
    dashboard.style.gridTemplateColumns="350px 1fr";
  }
});
addEventListener("load",()=>{
    const dashboard=document.getElementById('dashboard-container')
  if (window.innerWidth < 900) {
    navbar.classList.remove("show")
    navbar.classList.add("hide");
    dashboard.style.gridTemplateColumns="0 1fr";
  } else {
    navbar.classList.remove("show");
    navbar.classList.remove("hide");
    dashboard.style.gridTemplateColumns="350px 1fr";

  }
})

